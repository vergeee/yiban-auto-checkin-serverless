package com.verge.entity;

import lombok.Data;

import java.util.List;

/**
 * @Author Verge
 * @Date 2021/5/23 16:35
 * @Version 1.0
 */
@Data
public class EmailMessage {
    private String subject;
    private String content;
    private List<String> mailbox;
}