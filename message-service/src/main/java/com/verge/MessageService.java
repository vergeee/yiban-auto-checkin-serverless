package com.verge;

import cn.hutool.extra.mail.MailUtil;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.verge.common.R;
import com.verge.entity.EmailMessage;


/**
 * @Author Verge
 * @Date 2021/5/23 16:35
 * @Version 1.0
 */
public class MessageService implements RequestHandler<APIGatewayProxyRequestEvent, R> {
    Gson gson = new GsonBuilder().setPrettyPrinting().create();

    @Override
    public R handleRequest(APIGatewayProxyRequestEvent input, Context context) {
        LambdaLogger logger = context.getLogger();
        EmailMessage emailMessage = gson.fromJson(input.getBody(),EmailMessage.class);
        logger.log("EmailMessage: "+emailMessage);

        try {
            MailUtil.send(emailMessage.getMailbox(),emailMessage.getSubject(),emailMessage.getContent(),false);
        } catch (Exception e) {
            return R.error(e.getMessage());
        }

        return R.ok("success");
    }
}